// Temperature sensor libraries
#include <dht11.h>

// LCD screen libraries
#include "Wire.h" // For I2C
#include "LCD.h" // For LCD
#include "LiquidCrystal_I2C.h" // Added library*

// SD card libraries
#include <SPI.h>
#include <SD.h>


// Create temp sensor instance
#define NUM_SENSORS 5
dht11 sensors[NUM_SENSORS];
int sensorInputs[NUM_SENSORS] = {2, 3, 4, 5, 6};


//Set the pins on the I2C chip used for LCD connections
//ADDR,EN,R/W,RS,D4,D5,D6,D7
LiquidCrystal_I2C lcd(0x27,2,1,0,4,5,6,7); // 0x27 is the I2C bus address of LCD
#define CHIP_SELECT 10
#define LCD_WIDTH 20
#define LCD_HEIGHT 4
#define LCD_BACKLIGHT_PIN 3

// Used for measurement time stamps
unsigned long startTime;

// the setup function runs once when you press reset or power the board
void setup() {
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  // Setup LCD module
  Serial.println("Initializing LCD...");  
  lcd.begin (LCD_WIDTH, LCD_HEIGHT);
  lcd.setBacklightPin(LCD_BACKLIGHT_PIN,POSITIVE); // BL, BL_POL
  lcd.setBacklight(HIGH);
  lcd.print("Hello, World!");
  Serial.println("LCD initialized");

  // Setup SD card
  Serial.println("Initializing SD card...");
  // see if the card is present and can be initialized:
  if (!SD.begin(CHIP_SELECT)) {
    Serial.println("Card failed, or not present");
    // don't do anything more:
    return;
  }
  Serial.println("SD card initialized.");

  // Create csv file with correct header
  int i;
  String tempString = "Temp";
  String humString = "Hum";
  for(i = 1; i < NUM_SENSORS; i++){
    tempString += ",Temp";
    humString += ",Hum";
  }
  String headerString = "";
  headerString.concat("Time,");
  headerString.concat(tempString);
  headerString.concat(",");
  headerString.concat(humString);
  Serial.println(headerString);
  SDcardWriteLine("data.csv", headerString);
}


// the loop function runs over and over again forever
void loop() {
  int tempValues[NUM_SENSORS];
  int humValues[NUM_SENSORS];

  updateMeasurements(tempValues,humValues);
  updateLCD(tempValues, humValues);
  updateData(tempValues, humValues);
  
  delay(3000);                       // wait for 3 seconds
}

void updateMeasurements(int tempValues[], int humValues[]){
  int i;
  for(i = 0; i < NUM_SENSORS; i++){
    int chk = sensors[i].read(sensorInputs[i]);
    Serial.print("Sensor ");
    Serial.print(i);
    Serial.print(" status:");
    switch (chk)
    {
      case DHTLIB_OK: 
      Serial.println("OK"); 
      break;
      case DHTLIB_ERROR_CHECKSUM: 
      Serial.println("Checksum error"); 
      break;
      case DHTLIB_ERROR_TIMEOUT: 
      Serial.println("Time out error"); 
      break;
      default: 
      Serial.println("Unknown error"); 
      break;
    }
    Serial.print("Temperature(C): ");
    Serial.println(sensors[i].temperature);
    Serial.print("Humidity(%): ");
    Serial.println(sensors[i].humidity);

    tempValues[i] = sensors[i].temperature;
    humValues[i] = sensors[i].humidity;
  }
}

void updateLCD(int tempValues[], int humValues[]){
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("T:");
  lcd.setCursor(0,1);
  lcd.print("H:");

  int i;
  for(i = 0; i < NUM_SENSORS; i++){
    char temp[4];
    sprintf(temp, "%3d", tempValues[i]);
    lcd.setCursor(4 + 3*i,0);
    lcd.print(temp);
    char hum[4];
    sprintf(hum, "%3d", humValues[i]);
    lcd.setCursor(4 + 3*i,1);
    lcd.print(hum);
  }
}

void updateData(int tempValues[], int humValues[]){
  int i;
  String tempString = String(tempValues[0]);
  String humString = String(humValues[0]);
  for(i = 1; i < NUM_SENSORS; i++){
    tempString += ",";
    tempString += String(tempValues[i]);
    humString += ",";
    humString += String(humValues[i]);
  }
  String dataString = "";
  dataString.concat(String(millis()));
  dataString.concat(",");
  dataString.concat(tempString);
  dataString.concat(",");
  dataString.concat(humString);
  SDcardWriteLine("data.csv", dataString);
}

void SDcardWriteLine(String fileName, String dataString){
  File dataFile = SD.open(fileName, FILE_WRITE);

  // if the file is available, write to it:
  if (dataFile) {
    dataFile.println(dataString);
    dataFile.close();
    // print to the serial port too:
    Serial.print("Successfully wrote string: ");
    Serial.println(dataString);
  }
  // if the file isn't open, pop up an error:
  else {
    Serial.print("Error opening ");
    Serial.println(fileName);
  }
}
